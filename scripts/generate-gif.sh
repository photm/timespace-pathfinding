#!/bin/sh

if ! congif=$(which congif); then
    echo I need congif!  Get it from https://github.com/lecram/congif
    exit 1
fi

congifFont=$(dirname $congif)/misc-fixed-6x10.mbf

if [ $# != 1 ]; then
    echo Usage: $0 TRANSCRIPT-BASE-NAME
    echo Will convert the corresponding transcript in simulation-transcripts/
    echo to GIF using $congif
    exit 2
fi

transcriptBaseName=$1

convertTypescriptToGif() {
    local timingInput=$1
    local terminalInput=$2
    local gifOutput=$3
    $congif -w30 -h30 -m0.5 -l0 -f $congifFont -o $gifOutput $timingInput $terminalInput
}

base=simulation-transcripts/$transcriptBaseName

cp $base.timing $base.timing.2
echo '1.000000 0' >> $base.timing.2

convertTypescriptToGif $base.timing.2 $base.script $base.gif
