#!/bin/sh

#if ! chezScheme=$(which scheme-script); then
#    echo I need chez scheme!
#    exit 1
#fi
chezScheme=scheme-script

if [ $# != 2 ]; then
    echo Usage: $0 SCHEME-SCRIPT-FILE OUTPUT-TRANSCRIPT-BASE-NAME
    echo Will run SCHEME-SCRIPT-FILE using $chezScheme
    echo and record a typescript, writing to simulation-transcripts/
    echo using OUTPUT-TRANSCRIPT-BASE-NAME for the naming
fi

schemeScriptFile=$1
outputTranscriptBaseName=$2

recordTypescript() {
    local command=$1
    local timingOutput=$2
    local terminalOutput=$3
    script $terminalOutput -c "$command" -t 2> $timingOutput
}

mkdir -p simulation-transcripts

outBase=simulation-transcripts/$outputTranscriptBaseName

recordTypescript "$chezScheme $schemeScriptFile" $outBase.timing $outBase.script
