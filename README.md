# Collision-free timespace pathfinding

(This document describes the future as if it was present, everything you read
here is still WIP.)

This repo contains a library suitable for an interesting form of pathfinding, as
well as an ascii-art visualization tool and some example scenarios.

## General code structure

* Everything major has tests in `tests/`.  The tests are meant to be
  read as documentation;  there's comments along with the test cases.
* To decrease coupling, some things are implemented in an object-oriented
  way.  Since implementing interfaces (called capabilities here) is not
  enforced by the type system (which would be hard to do right anyway),
  it is enforced by tests.  These tests can be found along with the
  other code in `lib/` and are performed both at runtime and at test-time.

## Thougts and Pointers

* Possibly investigate [Lifelong Planning `A*`][1], as it sounds from the
  abstract it could fit the recomputation problem in case of path priority
  changes.

[1]: http://www.sciencedirect.com/science/article/pii/S000437020300225X?via%3Dihub
