(library (tests graph)
    (export run)
    (import
        (rnrs)
        (tests library)
        (lib base-objects)
        (lib objects)
        (lib graph))

    (define (run)
        (check (node-info (mk-node 'a)) => 'a)
        (check (node-relations (mk-node 'a)) => '())

        (check-capable (mk-node 'a) node)
        (check-capable (mk-node 'a (mk-relation 'r 15 (mk-node 'n))) node)
        (check-capable (mk-relation 'r 15 (mk-node 'n)) relation)

        (check (relation-cost (mk-relation 'r 15 (mk-node 'n))) => 15)
        (check (node-info (relation-node (mk-relation 'r 15 (mk-node 'n)))) => 'n)
))
