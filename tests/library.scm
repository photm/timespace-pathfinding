(library (tests library)
  (export check check-report-and-exit prepare)
  (import
    (rnrs)
    (rename (srfi :78) (check %check)))

  (define (prepare)
    ;; Don't report successful checks except in the summary
    (check-set-mode! 'report-failed))

  ;; These modes are supported by check:
  ; off: do not execute any of the checks,
  ; summary: print only summary in (check-report) and nothing else,
  ; report-failed: report failed checks when they happen, and in summary,
  ; report: report every example executed.

  ;; This global variable holds the number of tests that were ran
  (define %test-count 0)

  (define (%increase-test-count)
    (set! %test-count (+ 1 %test-count)))

  ;; Define a check macro that calls %check from srfi/78, but increases %test-count first
  (define-syntax check
    (syntax-rules ()
      ((check . rest)
       (begin (%increase-test-count)
              (%check . rest)))))

  ;; This should be called at the bottom of test files
  (define (check-report-and-exit)
    (check-report)
    (exit (if (check-passed? %test-count) 0 1))))
