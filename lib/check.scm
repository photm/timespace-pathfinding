(library (lib check)
    (export check)
    (import (rnrs) (rnrs io simple))

    (define (%proc trace expression thunk equal equal-expr expected-result)
        (define equal-expr*
            (and (not (eq? equal? equal))
                equal-expr))
        (define (display-arrow result comment output-port)
            (if equal-expr*
                (begin
                    (display "(=> " output-port)
                    (display equal-expr* output-port)
                    (display ")") output-port)
                (display "=>" output-port))
            (display " " output-port) (display result output-port)
            (display " ; " output-port) (display comment output-port)
            (newline output-port))
        (define (display-message actual-result output-port)
            (display "Runtime check failed." output-port) (newline output-port)
            (display expression output-port) (display " " output-port)
            (display-arrow actual-result "Actual result" output-port)
            (display "; But that's wrong, instead:" output-port)
            (newline output-port) (display "    " output-port)
            (display-arrow expected-result "Expected result" output-port))
        (let ((actual-result (thunk)))
            (if (not (equal actual-result expected-result))
                (begin
                    (let ((message (call-with-string-output-port
                        (lambda (output-port) (display-message actual-result output-port)))))
                        (error trace message))))))

    (define-syntax check
        (syntax-rules (=>)
            ((check trace expr => expected)
                (check trace expr (=> equal?) expected))
            ((check trace expr (=> equal) expected)
                (%proc trace 'expr (lambda () #f expr) equal 'equal expected)))))
