(library (lib graph)
    (export
        mk-node
        node
        node?
        node-info
        node-relations
        node=?
        relation
        mk-relation
        relation-cost
        relation-node)
    (import
        (rnrs)
        (rnrs io simple)
        (lib check)
        (lib utils)
        (lib objects)
        (lib base-objects)
        (only (srfi :1) fold)
        (srfi :26))

    (define-capability (node trace object)
        (check trace (node? object) => #t)
        (check trace (node=? object object) => #t)
        (check trace (ca object 'relations 'wrapper? 'raw) => #t)
        (check trace (list? (ca object 'relations 'raw)) => #t)
        (check trace (ca object 'info 'wrapper? 'raw) => #t))

    (define-capability (relation trace object)
        (check trace (object? object) => #t)
        (check trace (eq? #t (ca object 'node 'wrapper? 'raw)) => #f)
        (check trace (ca object 'info 'wrapper? 'raw) => #t))

    (define (mk-node info . relations)
        (let ((this (store)))
            (ca this 'capabilities '= (wrap (list node)))
            (ca this 'info '= (wrap info))
            (ca this 'relations '= (wrap relations))
            (ca this '=? '= (lambda (other)
                (and (ca this 'info '=? (ca other 'info))
                    (ca other 'info '=? (ca this 'info)))))
            this))

    (define (node? object)
        (and (object? object)
            (eq? #t (ca object 'capabilities 'contains? node 'raw))))

    (define (node-info node)
        (ca node 'info 'raw))

    (define (node-relations node)
        (ca node 'relations 'raw))

    (define (node=? node-a node-b)
        (and (node? node-a) (node? node-b)
            (ca node-a '=? node-b 'raw)
            (ca node-b '=? node-a 'raw)))

    (define (mk-relation info cost node)
        (let ((this (store)))
            (ca this 'capabilities '= (wrap (list relation)))
            (ca this 'cost '= (wrap cost))
            (ca this 'info '= (wrap info))
            (ca this 'node '= node)
            this))

    (define (relation-cost relation)
        (ca relation 'cost 'raw))

    (define (relation-node relation)
        (relation 'node)))
