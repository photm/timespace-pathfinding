(library (lib utils)
    (export
        const
        each-eq
        range-upper-exclusive
        mk-priority-queue
        priority-queue-first
        priority-queue-first-priority
        priority-queue-empty?
        priority-queue-push
        priority-queue-pop
        mk-map
        map-insert
        map-get-using
        map-get)
   (import (rnrs) (rnrs sorting) (only (srfi :1) delete-duplicates) (srfi :26))

    (define (const x)
        (lambda (_) x))

    (define (and-prod booleans)
        (if (null? booleans)
            #t
            (and (car booleans)
                (and-prod (cdr booleans)))))

    (define (each-eq element=?)
        (lambda (list-a list-b)
            (and (list? list-a) (list? list-b)
                (= (length list-a) (length list-b))
                (and-prod (map element=? list-a list-b)))))

    (define (range-upper-exclusive from to)
        (if (>= from to)
            '()
            (cons from (range-upper-exclusive (+ 1 from) to))))

    (define (mk-priority-queue < =)
        (%mk-priority-queue < = (list)))

    (define (priority-queue-first priority-queue)
        (%item-entry (car (%priority-queue-items priority-queue))))

    (define (priority-queue-first-priority priority-queue)
        (%item-priority (car (%priority-queue-items priority-queue))))

    (define (priority-queue-empty? priority-queue)
        (null? (%priority-queue-items priority-queue)))

    (define (priority-queue-push priority entry priority-queue)
        (null? (cdr priority-queue))
        (%priority-queue-fixup
            (%mk-priority-queue
                (%priority-queue-comparator priority-queue)
                (%priority-queue-equivalence priority-queue)
                (cons
                    (%mk-item priority entry)
                    (%priority-queue-items priority-queue)))))

    (define (priority-queue-pop priority-queue)
        (%mk-priority-queue
            (%priority-queue-comparator priority-queue)
            (%priority-queue-equivalence priority-queue)
            (cdr (%priority-queue-items priority-queue))))

    (define (%mk-priority-queue < = items)
        (cons* < = items))

    (define (%priority-queue-items priority-queue)
        (cddr priority-queue))

    (define (%priority-queue-comparator priority-queue)
        (car priority-queue))

    (define (%priority-queue-equivalence priority-queue)
        (cadr priority-queue))

    (define (%mk-item priority entry)
        (cons priority entry))

    (define (%item-priority item)
        (car item))

    (define (%item-entry item)
        (cdr item))

    (define (%priority-queue-fixup priority-queue)
        (let ((res (%mk-priority-queue
            (%priority-queue-comparator priority-queue)
            (%priority-queue-equivalence priority-queue)
            ;; Beware: delete-duplicates is O(n^2)!
            (delete-duplicates
                (list-sort (lambda (item-a item-b)
                    ((%priority-queue-comparator priority-queue)
                        (%item-priority item-a)
                        (%item-priority item-b)))
                    (%priority-queue-items priority-queue))
                (lambda (item-a item-b)
                    ((%priority-queue-equivalence priority-queue) (%item-entry item-a) (%item-entry item-b))))
                    )))
            res))

    (define (mk-map)
        (list))

    (define (map-insert key value the-map)
        (cons (cons key value) the-map))
        ;; Beware: delete-duplicates is O(n^2)!
        ;(delete-duplicates (cons (cons key value) the-map)
            ;(lambda (pair-a pair-b)
                ;(equal? (car pair-a) (car pair-b)))))

    (define (map-get-using key the-map =)
        (let ((key-value-pair-or-false (assp (cut = key <>) the-map)))
            (and key-value-pair-or-false (cdr key-value-pair-or-false))))

    (define (map-get key the-map)
        (map-get-using key the-map equal?)))
